[TOC]

**解决方案介绍**
===============
该解决方案可以帮助您快速在华为云弹性云服务器上部署源码编译环境，通过云服务器弹性伸缩的特性，结合Jenkins的持久化构建的能力以及Gerrit代码仓库，帮助企业快速部署各类复杂的编译环境，以最低的编译成本，实现日常的编译工作。Jenkins是一个开源软件项目，是基于Java开发的一种持续集成工具，用于监控持续重复的工作，旨在提供一个开放易用的软件平台，使软件项目可以进行持续集成。Gerrit 是一个基于 web 的代码评审工具, 它基于 git 版本控制系统，旨在提供一个轻量级框架, 用于在代码入库之前对每个提交进行审阅。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/deploy-a-source-code-build-system-based-on-jenkins.html

**架构图**
---------------
![架构图](./document/deploy-a-source-code-build-system-based-on-jenkins.png)

**架构描述**
---------------
该解决方案会部署如下资源：
1. 创建两台弹性云服务器，分别部署Gerrit代码仓和Jenkins Master节点；
2. 用户可以使用镜像服务，提前准备编译需要的操作系统环境；
3. 配置编译节点，在Jenkins Master节点中配置华为云插件，实现动态创建、释放ECS编译节点；
4. 创建对象存储桶，用来存储编译结果。

**组织结构**
---------------

``` lua
huaweicloud-solution-deploy-a-source-code-build-system-based-on-jenkins
├── deploy-a-source-code-build-system-based-on-jenkins.tf.json -- 资源编排模板
├── userdata
    ├── install_gerrit.sh  -- gerrit代码仓库安装
	├── install_jenkins.sh  -- jenkins安装
```
**开始使用**
---------------
***访问gerrit并创建代码仓库***

1. 在该方案创建的堆栈的“输出”标签页中，复制gerrit访问地址。

图1 gerrit访问地址

![ gerrit访问地址](./document/readme-image-001.png)
2. 在浏览器中访问复制的gerrit地址，并单击“Sign in”进行登录。

图2 gerrit访问地址

![ 访问gerrit](./document/readme-image-002.png)
3. 在登录页面单击“Sign in with a Launchpad ID”。

图3  登录gerrit1

![ 登录gerrit1](./document/readme-image-003.png)
4. 在Ubuntu One页面中，输入电子邮件地址及密码进行登录（如果没有账号需要点击右上角“登录或创建账号”进行账号创建）。

图4  登录gerrit2

![ 登录gerrit2](./document/readme-image-004.png)
5. 在弹出的界面中，单击“是的，我要登录”。

图5  登录gerrit3

![ 登录gerrit3](./document/readme-image-005.png)
6. 单击“BROWSE”-“Repositories”-“CREATE NEW”，输入Repository name、Default Branch等信息后，单击“CREATE”。

图6   创建代码仓库

![ 创建代码仓库](./document/readme-image-006.png)


***访问Jenkins并配置插件***

1.在该方案创建的堆栈的“资源”标签页中，单击生成的Jenkins服务器。

图7   访问Jenkins服务器

![ 访问Jenkins服务器](./document/readme-image-007.png)

2.单击“远程登录”，在CloudShell登录方式下点击“CloudShell登录”。

图8   Jenkins服务器登录

![ Jenkins服务器登录](./document/readme-image-008.png)

3.输入账号密码后，单击“连接”。

图9   Jenkins服务器连接

![ Jenkins服务器连接](./document/readme-image-009.png)

4.输入命令“cat /app/jenkins_home/secrets/initialAdminPassword”获取admin登录密码并复制。

图10   获取Jenkins admin密码

![ 获取Jenkins admin密码](./document/readme-image-010.png)

5.在该方案创建的堆栈的“输出”标签页中，复制Jenkins访问地址。

图11   Jenkins访问地址

![ Jenkins访问地址](./document/readme-image-011.png)

6.在浏览器中访问复制的Jenkins地址，粘贴步骤4中获取到的admin登录密码，点击“继续”。

图12   Jenkins平台登录

![ Jenkins平台登录](./document/readme-image-012.png)

7.选择“安装推荐的插件”或“选择插件来安装”进行插件安装。（此处以安装推荐的插件为例）。

图13   自定义Jenkins

![ 自定义Jenkins](./document/readme-image-013.png)

图14   等待插件安装

![ 等待插件安装](./document/readme-image-014.png)

8.等待插件安装完成，可以选择填写管理员信息进行管理员用户创建，也可以单击“使用admin账户继续”（此处以使用admin账户继续访问为例）。

图15   创建管理员

![ 创建管理员](./document/readme-image-015.png)

9.在实例配置页面进行Jenkins URL配置，单击“保存并完成”。

图16   配置Jenkins URL

![ 配置Jenkins URL](./document/readme-image-016.png)

10.Jenkins就绪后，单击“开始使用Jenkins”。

图17   Jenkins就绪

![ Jenkins就绪](./document/readme-image-017.png)

图18   访问Jenkins平台
![ 访问Jenkins平台](./document/readme-image-018.png)

11.请参考“HuaweiCloudEcs插件配置”文档进行HuaweiCloudEcs插件配置，实现自动创建华为云ECS作为Jenkins集群的slave节点。

***Git Test***

